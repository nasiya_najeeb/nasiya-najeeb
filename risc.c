//Program to find Range,Interquartile Range,SD,CV
/***************************************************************
*******Program to find Range,Interquartile Range,SD,CV*******
****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#define MAXSIZE 1000
FILE *fop;
int range_func(int[],int);
int iqr_func(int[],int);
float sd_func(int[],int);
float cv_func(int[],int);
void main(char argc, char **argv)
{
	FILE *fp;
        char arr[100];
	char buf[1000];
	char *value;
	char *inputval[MAXSIZE];
        int found=0;
	int i=0,j=0,a[MAXSIZE],n=0,k,temp,choice,range,iqr;
	float sd,cv;

	fp = fopen(argv[1],"r");
	fop = fopen("output.c","a+");

	while (fgets(buf,1000, fp)!=NULL)
	{
		value = strtok(buf," ,:;\n\t");
		while (value != NULL)
		{
	    	inputval[i] = strdup(value);
	    	value = strtok (NULL, " ,:;\n\t");
	    	i++;
		}

	}

	n = i;
	for(k=0; k<n; k++)
	{
		a[k] = atof(inputval[k]);
	}

	//Sort the Input array
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
			}
		}
	}
	do {
	printf("\n\t1.Range \n\t2.Interquartile range \n\t3.Standard Deviation \n\t4.Coefficient of Variation\n\t5. Write to File\n\t6.Exit\n\tEnter your choice: ");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1 : range=range_func(a,n);
			 printf("\t\t\nRange = %d",range);
			 break;
		case 2 : iqr=iqr_func(a,n);
			 printf("\t\tInterquartile range = %d",iqr);
			 break;
		case 3 : sd=sd_func(a,n);
			 printf("\t\tStandard Deviation = %f",sd);
			 break;
		case 4 : cv=cv_func(a,n);
			 printf("\t\tCoefficient of variation = %f",cv);
			 break;
		case 5 :
		 	 range=range_func(a,n);
		 	 iqr=iqr_func(a,n);
		 	 sd=sd_func(a,n);
		 	 cv=cv_func(a,n);
                         if(fop){
                                 fclose(fop);
                                 fop = fopen("output.c","a+");}
                         while (fgets(arr,100, fop)!=NULL)
	                 {
                           if((strstr(arr, "Range")) != NULL) {
                           found=1;
                          }
                          }
                          if(found==0)
                          {
	 	            fprintf(fop,"\nRange = %d \n",range);
		 	    fprintf(fop,"IQR = %d \n",iqr);
		 	    fprintf(fop,"SD = %f \n",sd);
		 	    fprintf(fop,"CV = %f \n",cv);
                          }break;

		case 6:
		 	 exit(0);
		default :printf("\n Wrong Choice");
	}

}while(choice!=6);
}

//Range
int range_func(int a[],int n){
	int range=a[n-1]-a[0];
	return range;
}

//Interquartile Range
int iqr_func(int a[], int n)
{
	int total;
	if(n%2==0){
		total = n;
	}
	else{
		total = n+1;
	}
	int lower = (.25)*total;
	int upper = (.75)*total;
	float lower_value = (a[lower]+a[lower+1])/2;
	float upper_value = (a[upper]+a[upper+1])/2;
	float total_value = upper_value-lower_value;
	return total_value;
}

//Standard Deviation
float sd_func(int a[], int n){
	float avg,ch,var;
	float sd,b[50];
	int i;
	for(i=0;i<n;i++)
			 avg=avg+a[i];
			 avg=avg/n;
			 for(i=0;i<n;i++)
			 {
				ch=a[i]-avg;
				ch=ch*ch;
				b[i]=ch;
			 }
			 for(i=0;i<n;i++)
				var=var+b[i];
			 var=var/n;
			 sd=sqrt(var);
			return sd;

}

//Coefficient of Variation
float cv_func(int a[], int n)
{
	int i;
	float avg;
	float sd,cv;
	for(i=0;i<n;i++)
		avg=avg+a[i];
	avg=avg/n;
	sd= sd_func(a,n);
	cv=sd/avg;
	return cv;
}
nasiya najeeb
